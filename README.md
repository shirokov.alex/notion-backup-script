[Automated Notion backups](https://medium.com/@arturburtsev/automated-notion-backups-f6af4edc298d)

Improvements of fork:
- Changed default branch from master to main
- Fixed incorrect encoding of file names with non-latin symbols
- Don't commit then no changes
- Use gitlab user name instead of id
- Remove guids in file and directory names